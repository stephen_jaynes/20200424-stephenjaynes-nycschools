//
//  XCTestCase.swift
//  20200424-StephenJaynes-NYCSchoolsTests
//
//  Created by STEPHEN D JAYNES on 4/24/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import XCTest

extension XCTestCase {
    
    func loadStubFromBundle(withName name: String, extension: String) -> Data {
        let bundle = Bundle(for: classForCoder)
        let url = bundle.url(forResource: name, withExtension: `extension`)
        
        return try! Data(contentsOf: url!)
    }
}
