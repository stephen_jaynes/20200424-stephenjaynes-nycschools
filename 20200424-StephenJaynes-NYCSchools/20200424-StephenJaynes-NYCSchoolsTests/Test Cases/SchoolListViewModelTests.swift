//
//  SchoolListViewModelTests.swift
//  20200424-StephenJaynes-NYCSchoolsTests
//
//  Created by STEPHEN D JAYNES on 4/24/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import XCTest
@testable import _0200424_StephenJaynes_NYCSchools

class SchoolListViewModelTests: XCTestCase {
    
    //--- Did not have time to add all test cases for this view model,
    //--- nor for the SchoolDetailViewModel

    var viewModel: SchoolListViewModel!
    
    override func setUpWithError() throws {
        let nycSchools = self.schoolsByBorough()
        self.viewModel = SchoolListViewModel(with: nycSchools)
    }

    override func tearDownWithError() throws {
    }
    
    func testNumberOfRowsForSection0() {
        XCTAssertEqual(self.viewModel.numberOfRowsFor(section: 0), 2)
    }
    
    func testNumberOfRowsForSection1() {
        XCTAssertEqual(self.viewModel.numberOfRowsFor(section: 1), 3)
    }
    
    func testNumberOfRowsForSection2() {
        XCTAssertEqual(self.viewModel.numberOfRowsFor(section: 2), 1)
    }
    
    func testNumberOfRowsForSection3() {
        XCTAssertEqual(self.viewModel.numberOfRowsFor(section: 3), 3)
    }
    
    func testNumberOfRowsForSection4() {
        XCTAssertEqual(self.viewModel.numberOfRowsFor(section: 4), 1)
    }
    
    func testTitleForHeaderInSection0() {
        XCTAssertEqual(self.viewModel.titleForHeaderIn(section: 0), "BRONX")
    }
    
    func testTitleForHeaderInSection1() {
        XCTAssertEqual(self.viewModel.titleForHeaderIn(section: 1), "BROOKLYN")
    }
    
    func testTitleForHeaderInSection2() {
        XCTAssertEqual(self.viewModel.titleForHeaderIn(section: 2), "MANHATTAN")
    }
    
    func testTitleForHeaderInSection3() {
        XCTAssertEqual(self.viewModel.titleForHeaderIn(section: 3), "QUEENS")
    }
    
    func testTitleForHeaderInSection4() {
        XCTAssertEqual(self.viewModel.titleForHeaderIn(section: 4), "STATEN IS")
    }
}

// MARK: - Helper Methods -

extension SchoolListViewModelTests {
    
    fileprivate func schoolsByBorough() -> [String: [NYCSchool]] {
        let data = loadStubFromBundle(withName: "nycSchools", extension: "json")
        let decoder = JSONDecoder()
        let nycSchools = try! decoder.decode(NYCSchools.self, from: data)
        
        //--- Sort schools by borough and then by name, and separate by borough
        //--- to enable borough headers

        return self.sortSchoolsByBoroughAndByName(schools: nycSchools)
    }
    
    fileprivate func sortSchoolsByBoroughAndByName(schools: [NYCSchool]) -> [String: [NYCSchool]] {
        let sortedSchools = schools.sorted { (lhs: NYCSchool, rhs: NYCSchool) -> Bool in
            if lhs.borough == rhs.borough {
                return lhs.schoolName! < rhs.schoolName!
            }
            return (lhs.borough!) < (rhs.borough!)
        }
        return self.createSchoolsByBoroughDictionary(using: sortedSchools)
    }
    
    fileprivate func createSchoolsByBoroughDictionary(using schools: [NYCSchool]) -> [String: [NYCSchool]] {
        var schoolsByBorough = [String: [NYCSchool]]()
        var borough = schools.first?.borough
        var schoolsInBorough = [NYCSchool]()
        
        for school in schools {
            if school.borough != borough {
                schoolsByBorough[borough!] = schoolsInBorough
                schoolsInBorough.removeAll()
                borough = school.borough
            }
            schoolsInBorough.append(school)
        }
        schoolsByBorough[borough!] = schoolsInBorough

        return schoolsByBorough
    }
}
