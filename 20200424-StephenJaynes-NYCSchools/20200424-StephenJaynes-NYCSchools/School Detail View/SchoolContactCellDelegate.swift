//
//  SchoolContactCellDelegate.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/24/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

protocol SchoolContactCellDelegate: AnyObject {
    func schoolContactCell(_ cell: UITableViewCell, didTapPhoneButton phoneButton: UIButton)
}
