//
//  SchoolDetailViewController.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/23/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController, Dialable {

    // MARK: - Public Properties -
    
    var viewModel: SchoolDetailViewModel!
        
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Private Properties -
    
    private let networkController = NetworkController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.retrieveSATScores()
        self.tableView.estimatedRowHeight = Constants.schoolCellEstimatedHeight
        self.tableView.rowHeight = UITableView.automaticDimension
    }
}

// MARK: - Private Helper Methods -

extension SchoolDetailViewController {
    fileprivate func retrieveSATScores() {
        self.toggleActivityIndicatorOn(true)
        self.networkController.getSATScoresForSchoolWith(dbn: self.viewModel.dbn,
                                                         completionHandler: {[weak self] (satScore, error) in
            DispatchQueue.main.async {
                self?.toggleActivityIndicatorOn(false)
                guard error == nil else {
                    self?.processNetworkError(error!)
                    return
                }
                self?.viewModel.updateSatScoreDataWith(satScore)
                self?.tableView.reloadData()
            }
        })
    }
    
    fileprivate func processNetworkError(_ error: Error) {
        let title = "SAT Score Download"
        let message = "Error occurred downloading SAT scores: \(error.localizedDescription)"
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let retryAction = UIAlertAction(title: "Retry", style: .default) {[weak self] (action) in
            self?.retrieveSATScores()
        }
        self.showAlertWithTitle(title, message: message, preferredStyle: .alert,
                                actions: [cancelAction, retryAction], textFieldConfigurationHandlers: nil,
                                animated: true, completion: nil)
    }
    
    fileprivate func toggleActivityIndicatorOn(_ activityIndicatorOn: Bool) {
        if activityIndicatorOn {
            let spinner = UIActivityIndicatorView(style: .gray)
            spinner.startAnimating()
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: spinner)
        } else {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
}

// MARK: - SchoolContactCellDelegate Methods -

extension SchoolDetailViewController: SchoolContactCellDelegate {
    func schoolContactCell(_ cell: UITableViewCell, didTapPhoneButton phoneButton: UIButton) {
        if let phoneNumber = phoneButton.currentTitle {
            self.dial(phoneNumber: phoneNumber)
        }
    }
}
