//
//  SchoolContactCell.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/23/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

class SchoolContactCell: UITableViewCell {
    
    // MARK: - Public Properties -
    
    weak var delegate: SchoolContactCellDelegate?

    // MARK: - IBOutlet Properties -
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityStateZipLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func phoneButtonTapped(_ sender: UIButton) {
        self.delegate?.schoolContactCell(self, didTapPhoneButton: sender)
    }
    
}
