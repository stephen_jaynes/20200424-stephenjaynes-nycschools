//
//  SchoolDetailViewModel.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/24/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import Foundation

struct SchoolDetailViewModel {
    var school: NYCSchool
    
    var satScoreData = SatScoreData()
    
    var dbn: String {
        return self.school.dbn ?? ""
    }
    
    var schoolName: String {
        return self.school.schoolName ?? "No name"
    }
    
    var schoolAddress: String {
        return self.school.streetAddress ?? "No address"
    }
    
    var cityStateZip: String {
        var cityStateZip = ""
        
        if let city = self.school.city {
            cityStateZip += city + ", "
        }
        cityStateZip += (self.school.stateCode ?? "") + " "
        cityStateZip += self.school.zipCode ?? " "
        return cityStateZip
    }
    
    var phoneNumber: String? {
        return self.school.phoneNumber
    }
    
    var mathScore: String {
        return self.satScoreData.mathScore
    }
    
    var readingScore: String {
        return self.satScoreData.readingScore
    }
    
    var writingScore: String {
        return self.satScoreData.writingScore
    }
    
    var overviewParagraph: String {
        return self.school.overviewParagraph ?? "No overview provided."
    }
    
    mutating func updateSatScoreDataWith(_ satScore: SatScore?) {
        self.satScoreData = SatScoreData(with: satScore)
    }
}
