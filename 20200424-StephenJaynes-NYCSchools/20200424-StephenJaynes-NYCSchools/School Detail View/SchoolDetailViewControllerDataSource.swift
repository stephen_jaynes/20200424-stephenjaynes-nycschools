//
//  SchoolDetailViewControllerDataSource.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/23/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

extension SchoolDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.cellsInSchoolDetailTableView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.schoolContactCellFor(tableView, indexPath: indexPath)
        case 1:
            return self.satScoreCellFor(tableView, indexPath: indexPath)
        case 2:
            return self.schoolOverviewCellFor(tableView, indexPath: indexPath)
        default:
            return UITableViewCell()
        }
    }
}

extension SchoolDetailViewController {
    fileprivate func schoolContactCellFor(_ tableView: UITableView,
                              indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolContactCell,
                                                 for: indexPath) as! SchoolContactCell
        cell.nameLabel.text = self.viewModel.schoolName
        cell.addressLabel.text = self.viewModel.schoolAddress
        cell.cityStateZipLabel.text = self.viewModel.cityStateZip
        
        if let phoneNumber = self.viewModel.phoneNumber {
            cell.phoneButton.setTitle(phoneNumber, for: .normal)
            cell.phoneButton.isHidden = false
            cell.delegate = self
        } else {
            cell.phoneButton.isHidden = true
            cell.delegate = nil
        }
        
        return cell
    }
    
    fileprivate func satScoreCellFor(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolSATCell,
                                                 for: indexPath) as! SchoolSATCell
        cell.mathScoreLabel.text = self.viewModel.mathScore
        cell.readingScoreLabel.text = self.viewModel.readingScore
        cell.writingScoreLabel.text = self.viewModel.writingScore
        return cell
    }
    
    fileprivate func schoolOverviewCellFor(_ tableView: UITableView,
                                           indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolOverviewCell,
                                                 for: indexPath) as! SchoolOverviewCell
        cell.overviewLabel.text = self.viewModel.overviewParagraph
        return cell
    }
}
