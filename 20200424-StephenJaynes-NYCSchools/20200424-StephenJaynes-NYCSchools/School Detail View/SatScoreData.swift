//
//  SatScoreData.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/23/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import Foundation

struct SatScoreData {
    var mathScore = ""
    var readingScore = ""
    var writingScore = ""
}

extension SatScoreData {
    
    //--- Some schools do not have an SAT scores record, so if no data is returned, set values accordingly
    
    init(with satScore: SatScore?) {
        if satScore == nil {
            self.mathScore = "Not Found"
            self.readingScore = "Not Found"
            self.writingScore = "Not Found"
            return
        }
        
        //--- Some schools have SAT score records with non-numeric data, set values accordingly
        
        if let mathScore = satScore?.satMathAvgScore, Int(mathScore) != nil {
            self.mathScore = mathScore.trimmingCharacters(in: .whitespacesAndNewlines)
        } else {
            self.mathScore = "Invalid data"
        }
        
        if let readingScore = satScore?.satCriticalReadingAvgScore, Int(readingScore) != nil {
            self.readingScore = readingScore.trimmingCharacters(in: .whitespacesAndNewlines)
        } else {
            self.readingScore = "Invalid data"
        }
        
        if let writingScore = satScore?.satWritingAvgScore, Int(writingScore) != nil {
            self.writingScore = writingScore.trimmingCharacters(in: .whitespacesAndNewlines)
        } else {
            self.writingScore = "Invalid data"
        }
    }
}
