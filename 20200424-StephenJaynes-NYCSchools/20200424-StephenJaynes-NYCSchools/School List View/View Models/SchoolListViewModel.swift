//
//  SchoolListViewModel.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/24/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import Foundation

struct SchoolListViewModel {
    var schools = [String: [NYCSchool]]()
    
    var schoolSectionTitles = [String]()

    var numberOfSections: Int {
        return schools.count
    }

    init() { }
    
    init(with schools: [String: [NYCSchool]]) {
        self.schools = schools
        self.schoolSectionTitles = self.schools.keys.sorted()
    }
    
    func numberOfRowsFor(section: Int) -> Int {
        return self.schools[schoolSectionTitles[section]]!.count
    }
    
    func titleForHeaderIn(section: Int) -> String {
        return self.schoolSectionTitles[section]
    }
    
    func schoolNameFor(indexPath: IndexPath) -> String {
        let school = self.schoolForIndexPath(indexPath)
        return school.schoolName ?? "No name"
    }
    
    func phoneNumberFor(indexPath: IndexPath) -> String? {
        let school = self.schoolForIndexPath(indexPath)
        return school.phoneNumber
    }
    
    func schoolForIndexPath(_ indexPath: IndexPath) -> NYCSchool {
        let borough = self.schoolSectionTitles[indexPath.section]
        let boroughSchools = self.schools[borough]!
        return boroughSchools[indexPath.row]
    }
}
