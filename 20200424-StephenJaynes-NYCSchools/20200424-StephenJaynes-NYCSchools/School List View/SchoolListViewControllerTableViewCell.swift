//
//  SchoolListViewControllerTableViewCell.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/22/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

class SchoolListViewControllerTableViewCell: UITableViewCell {
    
    // MARK: - Public Properties -
    
    weak var delegate: SchoolListViewControllerTableViewCellDelegate?

    // MARK: - IBOutlet Properties -
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - @IBAction Methods -
    
    @IBAction func phoneButtonTapped(_ sender: UIButton) {
        self.delegate?.schoolListViewControllerTableViewCell(self, didTapPhoneButton: sender)
    }
    
}
