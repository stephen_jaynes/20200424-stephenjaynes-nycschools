//
//  SchoolListViewControllerTableViewCellDelegate.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/23/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

protocol SchoolListViewControllerTableViewCellDelegate: AnyObject {
    func schoolListViewControllerTableViewCell(_ cell: UITableViewCell, didTapPhoneButton phoneButton: UIButton)
}
