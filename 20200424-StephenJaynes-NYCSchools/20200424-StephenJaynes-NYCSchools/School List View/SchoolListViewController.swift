//
//  SchoolListViewController.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/22/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

class SchoolListViewController: UIViewController, Dialable {
    
    // MARK: - Public Properties -

    var viewModel = SchoolListViewModel()

    // MARK: - IBOutlet Properties -
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var loadingView: UIView!
    
    // MARK: - Private Properties -
    
    private let networkController = NetworkController()
    
    // MARK: - ViewController Lifecycle Methods -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "NYC Schools"
        self.retrieveSchoolData()
        self.tableView.estimatedRowHeight = Constants.schoolCellEstimatedHeight
        self.tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    // MARK: - Navigation Methods -
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.schoolDetailSegueName {
            if let selectedIndex = self.tableView.indexPathForSelectedRow {
                let controller = segue.destination as! SchoolDetailViewController
                let school = self.viewModel.schoolForIndexPath(selectedIndex)
                controller.viewModel = SchoolDetailViewModel(school: school)
            }
        }
    }
}

// MARK: - Private Helper Methods -

extension SchoolListViewController {
    fileprivate func retrieveSchoolData() {
        self.setLoadingViewTo(visible: true, animated: true)
        self.networkController.getAllSchoolsDataOnCompletion {[unowned self] (schools, error) in
            DispatchQueue.main.async {
                self.setLoadingViewTo(visible: false, animated: true)
                guard schools != nil, error == nil else {
                    self.processNetworkError(error!)
                    return
                }
                self.viewModel = SchoolListViewModel(with: schools!)
                self.tableView.reloadData()
            }
        }
    }
    
    fileprivate func processNetworkError(_ error: Error) {
        let title = "School File Download"
        let message = "Error occurred downloading school data: \(error.localizedDescription)"
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { (action) in
            self.retrieveSchoolData()
        }
        self.showAlertWithTitle(title, message: message, preferredStyle: .alert,
                                actions: [cancelAction, retryAction], textFieldConfigurationHandlers: nil,
                                animated: true, completion: nil)
    }
    
    fileprivate func setLoadingViewTo(visible: Bool, animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.25) {
                self.loadingView.alpha = visible ? 1.0 : 0.0
            }
        } else {
            self.loadingView.alpha = visible ? 1.0 : 0.0
        }
    }
}

// MARK: - SchoolListViewControllerTableViewCellDelegate Methods -

extension SchoolListViewController: SchoolListViewControllerTableViewCellDelegate {
    func schoolListViewControllerTableViewCell(_ cell: UITableViewCell, didTapPhoneButton phoneButton: UIButton) {
        if let phoneNumber = phoneButton.currentTitle {
            self.dial(phoneNumber: phoneNumber)
        }
    }
}
