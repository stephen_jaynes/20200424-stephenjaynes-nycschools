//
//  SchoolListViewControllerDataSource.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/22/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - UITableViewDataSource Methods -
    
    func numberOfSections(in tableView: UITableView) -> Int {
        self.viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRowsFor(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolCell, for: indexPath) as! SchoolListViewControllerTableViewCell
        cell.schoolNameLabel.text = self.viewModel.schoolNameFor(indexPath: indexPath)
        
        if let phoneNumber = self.viewModel.phoneNumberFor(indexPath: indexPath) {
            cell.phoneNumberLabel.text = "Phone:"
            cell.phoneButton.setTitle(phoneNumber, for: .normal)
            cell.phoneButton.isHidden = false
            cell.delegate = self
        } else {
            cell.phoneNumberLabel.text = ""
            cell.phoneButton.isHidden = false
            cell.delegate = nil
        }

        return cell
    }
    
    // MARK: - UITableViewDelegate Methods -
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.viewModel.titleForHeaderIn(section: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: Constants.schoolDetailSegueName, sender: self)
    }
}
