//
//  NYCJSonData.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/22/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import Foundation

struct NYCSchool: Codable {
    var dbn:                String?
    var schoolName:         String?
    var phoneNumber:        String?
    var overviewParagraph:  String?
    var schoolEmail:        String?
    var website:            String?
    var streetAddress:      String?
    var city:               String?
    var stateCode:          String?
    var zipCode:            String?
    var borough:            String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName         = "school_name"
        case phoneNumber        = "phone_number"
        case overviewParagraph  = "overview_paragraph"
        case schoolEmail        = "school_email"
        case website
        case streetAddress      = "primary_address_line_1"
        case city
        case stateCode          = "state_code"
        case zipCode            = "zip"
        case borough
    }
    
    //--- Added initializer in order to handle nil borough value -- sorting by school name
    //--- within borough will fail with nil borough name, so plugging city into borough name
    //--- if borough name is nil
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
        self.schoolName = try container.decodeIfPresent(String.self, forKey: .schoolName)
        self.phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        self.overviewParagraph = try container.decodeIfPresent(String.self, forKey: .overviewParagraph)
        self.schoolEmail = try container.decodeIfPresent(String.self, forKey: .schoolEmail)
        self.website = try container.decodeIfPresent(String.self, forKey: .website)
        self.streetAddress = try container.decodeIfPresent(String.self, forKey: .streetAddress)
        self.city = try container.decodeIfPresent(String.self, forKey: .city)
        self.stateCode = try container.decodeIfPresent(String.self, forKey: .stateCode)
        self.zipCode = try container.decodeIfPresent(String.self, forKey: .zipCode)
        self.borough = try container.decodeIfPresent(String.self, forKey: .borough)?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if self.borough == nil {
            self.borough = self.city?.uppercased().trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        //--- Remove extraneous characters from schoolName and overview
        
        self.schoolName = self.removeCharactersFrom(string: self.schoolName)
        self.overviewParagraph = self.removeCharactersFrom(string: self.overviewParagraph)
    }
}

extension NYCSchool {
    fileprivate func removeCharactersFrom(string: String?) -> String? {
        if var newString = string {
            let charactersToRemove = "Â"
            newString.removeAll(where: { charactersToRemove.contains($0) })
            return newString
        }
        return string
    }
}

typealias NYCSchools = [NYCSchool]
