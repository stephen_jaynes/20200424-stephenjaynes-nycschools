//
//  SatScoreJsonData.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/23/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import Foundation

struct SatScore: Codable {
    var dbn:                        String?
    var schoolName:                 String?
    var numOfSatTestTakers:         String?
    var satCriticalReadingAvgScore: String?
    var satMathAvgScore:            String?
    var satWritingAvgScore:         String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName                 = "school_name"
        case numOfSatTestTakers         = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore            = "sat_math_avg_score"
        case satWritingAvgScore         = "sat_writing_avg_score"
    }
}

typealias SatScores = [SatScore]
