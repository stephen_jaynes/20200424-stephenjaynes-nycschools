//
//  Constants.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/22/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

enum Constants {
    static let schoolsURLString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let satScoreURLString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    
    static let schoolCell = "SchoolCell"
    static let schoolContactCell = "SchoolContactCell"
    static let schoolSATCell = "SchoolSATCell"
    static let schoolOverviewCell = "SchoolOverviewCell"
    
    static let schoolDetailSegueName = "ShowSchoolDetailViewController"
    
    static let cellsInSchoolDetailTableView = 3
    
    static let schoolCellEstimatedHeight: CGFloat = 37.0
}
