//
//  NetworkController.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/22/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import Foundation

enum NetworkControllerError: Error {
    case invalidURL(urlString: String)
    case jsonDecoderError
}

class NetworkController: NSObject {
    
    private lazy var defaultConfig: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        return config
    }()
    private lazy var session: URLSession = {
        return URLSession(configuration: self.defaultConfig)
    }()

    // MARK: - Public Methods -
    
    func getAllSchoolsDataOnCompletion(_ completionHandler: @escaping (_ results: [String: [NYCSchool]]?, Error?) -> Void) {
        guard let url = URL(string: Constants.schoolsURLString) else {
            let error = NetworkControllerError.invalidURL(urlString: Constants.schoolsURLString)
            completionHandler(nil, error)
            return
        }
        let urlRequest = URLRequest(url: url)
        let task = self.session.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, error == nil else {
                completionHandler(nil, error)
                return
            }
            let decoder = JSONDecoder()
            do {
                let nycSchools = try decoder.decode(NYCSchools.self, from: data)
                
                //--- Sort schools by borough and then by name, and separate by borough
                //--- to enable borough headers

                let indexedSchools = self.sortSchoolsByBoroughAndByName(schools: nycSchools)
                completionHandler(indexedSchools, nil)
            } catch {
                let error = NetworkControllerError.jsonDecoderError
                completionHandler(nil, error)
            }
        }
        task.resume()
    }
    
    func getSATScoresForSchoolWith(dbn: String,
                                   completionHandler: @escaping (_ results: SatScore?, Error?) -> Void) {
        let urlString = Constants.satScoreURLString + dbn
        print("urlString = \(urlString)")
        guard let url = URL(string: urlString) else {
            let error = NetworkControllerError.invalidURL(urlString: urlString)
            completionHandler(nil, error)
            return
        }
        let urlRequest = URLRequest(url: url)
        let task = self.session.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, error == nil else {
                completionHandler(nil, error)
                return
            }
            let decoder = JSONDecoder()
            do {
                let satScore = try decoder.decode(SatScores.self, from: data).first
                completionHandler(satScore, nil)
            } catch {
                let error = NetworkControllerError.jsonDecoderError
                completionHandler(nil, error)
            }
        }
        task.resume()
    }
}

extension NetworkController {
    fileprivate func sortSchoolsByBoroughAndByName(schools: [NYCSchool]) -> [String: [NYCSchool]] {
        let sortedSchools = schools.sorted { (lhs: NYCSchool, rhs: NYCSchool) -> Bool in
            if lhs.borough == rhs.borough {
                return lhs.schoolName! < rhs.schoolName!
            }
            return (lhs.borough!) < (rhs.borough!)
        }
        return self.createSchoolsByBoroughDictionary(using: sortedSchools)
    }
    
    fileprivate func createSchoolsByBoroughDictionary(using schools: [NYCSchool]) -> [String: [NYCSchool]] {
        var schoolsByBorough = [String: [NYCSchool]]()
        var borough = schools.first?.borough
        var schoolsInBorough = [NYCSchool]()
        
        for school in schools {
            if school.borough != borough {
                schoolsByBorough[borough!] = schoolsInBorough
                schoolsInBorough.removeAll()
                borough = school.borough
            }
            schoolsInBorough.append(school)
        }
        schoolsByBorough[borough!] = schoolsInBorough

        return schoolsByBorough
    }
}
