//
//  Dialable.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/23/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

protocol Dialable {
    func dial(phoneNumber: String)
}

extension Dialable {
    func dial(phoneNumber: String) {
        guard let telephoneURL = URL(string: "tel://\(phoneNumber)") else {
            return
        }
        UIApplication.shared.open(telephoneURL, options: [:]) { (flag) in
            
        }
    }
}
