//
//  ViewController+UserAlert.swift
//  20200424-StephenJaynes-NYCSchools
//
//  Created by STEPHEN D JAYNES on 4/22/20.
//  Copyright © 2020 STEPHEN D JAYNES. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlertWithTitle(_ title: String?, message: String?,
                            preferredStyle: UIAlertController.Style,
                            actions: [UIAlertAction],
                            textFieldConfigurationHandlers: [((UITextField) -> Void)]?,
                            animated: Bool, completion: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        
        for action in actions {
            alert.addAction(action)
        }

        if textFieldConfigurationHandlers != nil {
            for textFieldConfigurationHandler in textFieldConfigurationHandlers! {
                alert.addTextField(configurationHandler: textFieldConfigurationHandler)
            }
        }
        self.present(alert, animated: animated, completion: completion)
    }
}
